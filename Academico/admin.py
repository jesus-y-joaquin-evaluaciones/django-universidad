
from django.contrib import admin
from .models import Curso, Docente, Inscripcion
# Register your models here.

admin.site.register(Curso)
admin.site.register(Docente)
admin.site.register(Inscripcion)