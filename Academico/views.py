from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User 
from django.contrib import messages
from Academico.forms import LoginForm
from django.contrib.auth.decorators import login_required
from .forms import CreaCursoForm
from .models import Curso

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.error(request, 'Credenciales inválidas. Por favor, inténtalo de nuevo.')
    else:
        form = LoginForm()
    
    return render(request, 'login.html', {'form': form})

def logout_view(request):
    logout(request)
    return redirect('login')


def home_view(request):
    return render(request, 'home.html')

@login_required
def crea_curso(request):
    if request.method == 'GET':
        return render(request, 'crea_curso.html', {'form': CreaCursoForm})
    else:
        try:
            #print(request.POST)
            form= CreaCursoForm(request.POST) #crea formulario para guardar
            #nvo_curso = form.save(commit=False)
            #nvo_curso.user = request.user
            form.save()
            return redirect('home')
            # return render(request, 'crea_curso.html', {'form': CreaCursoForm})
        except ValueError:
            return render(request, 'crea_curso.html', {
                'form': CreaCursoForm,
                'error': 'Por favor entrega datos válidos'
                })

@login_required
def curso_detalle(request, id):
    if request.method =='GET':
        #curso= get_object_or_404(Curso, pk=id, user=request.user) si la tabla tiene registrado como foráneo el user, filtra por ususario logeado
        curso= get_object_or_404(Curso, pk=id)
        form = CreaCursoForm(instance=curso)
        return render(request,'curso_detalle.html',{'Curso':curso, 'form':form})
    else:
        try:
            curso= get_object_or_404(Curso, pk=id) #obtiene la tarea, esto exige agregar modulo
            #curso= get_object_or_404(Curso, pk=id, user=request.user) #si la tabla tiene foranea user sería filtrando además el user logeado
            form = CreaCursoForm(request.POST, instance= curso)
            form.save()
            return redirect('lista_cursos')
        except ValueError:
            return render(request,'curso_detalle.html',{'Curso':curso, 'form':form, 'error': "Error al actualizar Tabla Cursos"})

@login_required
def curso_borra(request, id):
    curso= get_object_or_404(Curso, pk=id)
    #curso= get_object_or_404(Curso, pk=id, user=request.user) si la tabla tiene registrado como foráneo el user, filtra por ususario logeado
    if request.method =='POST':
        curso.delete()
        return redirect('lista_cursos')
 

def crea_user(request):
    if request.method == 'GET':
        return render(request, 'crea_user.html',{
        'form': UserCreationForm
        })
    else:
        if request.POST['password1']==request.POST['password2']:
            try:
                user=User.objects.create_user(
                    username=request.POST['username'],
                    password=request.POST['password1'])
                user.save()
                #login(request, user)
                return redirect('home')
            except:
                return render(request, 'crea_user.html',{
                    'form': UserCreationForm,
                    "error":'Usuario ya existe'
                })
        return render(request,'crea_user.html',{
            'form': UserCreationForm,
            "error":'Password no coincide'
        })

@login_required            
def lista_cursos(request):
    listaC= Curso.objects.all()
    return render(request,'lista_cursos.html',{'cursos':listaC})
# si la tabla que quiero mostrar tiene user involucrado,
# debiera solo mostrar los que le corresponden
# y no los cursos creados por otro usuario, de ser así el codigo sería
#   listaC = Curso.objets.filter(request=request.user)
# el mismo return de antes