from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Curso (models.Model):
    nombre = models.CharField(max_length=30)
    creditos = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.nombre)

class Docente(models.Model):
    nombreDoc = models.CharField(max_length=30)
    edadDoc= models.IntegerField()
    # Otros campos y opciones de tu tabla

    def __str__(self):
        return str(self.nombreDoc)


class Inscripcion(models.Model):
    nombreDoc = models.ForeignKey(Docente, on_delete=models.CASCADE)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)
    #fecha_inscripcion = models.DateField(auto_now_add=True)
    # Otros campos adicionales si es necesario

    def __str__(self):
        return str(self.nombreDoc)