
from django import forms
from .models import Curso

class LoginForm(forms.Form):
    username = forms.CharField(label='Usuario', max_length=100)
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput)

class CreaCursoForm (forms.ModelForm):
    class Meta:
        model = Curso
        fields = ['nombre', 'creditos']