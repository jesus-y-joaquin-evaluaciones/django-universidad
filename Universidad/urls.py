from django.contrib import admin
from django.urls import path
from Academico.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('home/', home_view, name='home'),
    path('crea_curso/', crea_curso, name='crea_curso'),
    path('curso_detalle/<int:id>', curso_detalle, name='curso_detalle'),
    path('curso_detalle/<int:id>/borra', curso_borra, name='curso_borra'),
    path('crea_user/', crea_user, name='crea_user'),
    path('lista_cursos/', lista_cursos, name='lista_cursos'),  
]
